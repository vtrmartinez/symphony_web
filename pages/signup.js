// @ts-check
exports.SignUpPage = class SignUpPage {

    constructor(page){
        this.page = page;
        this.signup_menu = page.locator("(//span[contains(@class, 'mantine-1ryt1ht')])[1]");
        this.username_textfield = page.getByLabel('Username');
        this.email_textfield = page.getByLabel('Email address');
        this.password_textfield = page.getByLabel('Password');
        this.continue_button = page.getByRole('button', { name: 'Continue' });
    }

    async access_signup_page(){
        await this.signup_menu.click();
    }

    async fill_signup(username, email, password){
        await this.username_textfield.fill(username);
        await this.email_textfield.fill(email);
        await this.password_textfield.fill(password);
        await this.continue_button.click();
    }

}