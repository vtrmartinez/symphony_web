// @ts-check
const { expect } = require('@playwright/test');

exports.LoginPage = class LoginPage {
    
    constructor(page){
        this.page = page;
        this.login_menu = page.getByText('Login');
        this.username_textfield = page.getByLabel('Username or email address');
        this.password_textfield = page.getByLabel('Password');
        this.continue_button = page.getByRole('button', { name: 'Continue' });
    }
    
    async goto_website(){
        await this.page.goto('https://www.microapp.io/build');
        await expect(this.page).toHaveTitle(/Microapp/);
    }

    async access_login_page(){
        await this.login_menu.click();
    }

    async fill_login(username, password){
        await this.username_textfield.fill(username);
        await this.password_textfield.fill(password);
        await this.continue_button.click();
    }

    async verify_user_logged(email){
        await expect(this.page.getByText(email + "'s workspace")).toBeEnabled();
    }
}