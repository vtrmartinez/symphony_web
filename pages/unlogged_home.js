// @ts-check
const { expect } = require('@playwright/test');
exports.UnloggedHomePage = class UnloggedHomePage {

    constructor(page){
        this.page = page;
        this.apps_menu = page.locator("(//div[contains(@class, 'mantine-Text-root')])[2]");
        this.about_menu = page.locator("(//div[contains(@class, 'mantine-Text-root')])[3]");
        this.terms_of_use_link = page.locator("(//div[contains(@class, 'mantine-Text-root')])[12]");
        this.privacy_policy_link = page.locator("(//div[contains(@class, 'mantine-Text-root')])[13]");
        this.twitter_link = page.locator("(//div[contains(@class, 'mantine-Text-root')])[11]");
        this.dark_light_theme_button = page.locator("(//button[contains(@class, 'mantine-UnstyledButton')])[1]");
        this.light_theme_class = page.locator("(//span[contains(@class, 'mantine-1eha8hq')])[2]");
        this.dark_theme_class = page.locator("(//span[contains(@class, 'mantine-cp1rkb')])[2]");

        // Create Custom Component
        this.name_custom_component_textfield = page.getByRole('textbox', { name: 'e.g a tip calculator' });
        this.make_magic_button = page.getByRole('button', { name: 'generate component' });
        this.code_generator_scroller = page.locator('.cm-scroller');
        this.result_after_the_code = page.locator('div').nth(1);
        this.edit_button = page.getByRole('button', { name: 'edit component' });
    }

    async navigate_through_menus(){
        await expect(this.page.getByText('AI-Powered Microapp Generator')).toBeEnabled();
        await this.apps_menu.click();
        await expect(this.page).toHaveURL(/.*apps/);
        await this.about_menu.click();
        await expect(this.page).toHaveURL(/.*about/);
        await this.terms_of_use_link.click();
        await expect(this.page).toHaveURL(/.*terms-of-use/);
    }

    async verify_dark_or_light_themes(){
        await this.dark_light_theme_button.click();
        await expect(this.dark_theme_class).toBeVisible();
        await this.dark_light_theme_button.click();
        await expect(this.light_theme_class).toBeVisible();
    }

    async create_custom_component(name){
        await this.name_custom_component_textfield.fill(name);
        await this.make_magic_button.click();
        await expect(this.page).toHaveURL(/.*build/);
        await expect(this.code_generator_scroller).toBeVisible();
        await expect(this.result_after_the_code).toBeVisible();
        await expect(this.edit_button).toBeVisible();
    }
}