// @ts-check
const { test } = require('@playwright/test');
const {LoginPage} = require('../pages/login.js');
const {SignUpPage} = require('../pages/signup.js');


test('Successful Sign Up', async ({ page }) => {
  const Login = new LoginPage(page);
  const SignUp = new SignUpPage(page);
  const rand = Math.random().toString(36).substring(2,7);
  const email_value = `vtrmartinez+${rand}@gmail.com`;

  await Login.goto_website();
  await SignUp.access_signup_page();
  await SignUp.fill_signup(`vtr_${rand}`, email_value, 'Str0ng_P@ssw0rd');
  await Login.verify_user_logged(email_value);
});