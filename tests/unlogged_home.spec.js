// @ts-check
const { test } = require('@playwright/test');
const {LoginPage} = require('../pages/login.js');
const {UnloggedHomePage} = require('../pages/unlogged_home.js');


test('Navigate through menu items', async ({ page }) => {
  const Login = new LoginPage(page);
  const UnloggedHome = new UnloggedHomePage(page);

  await Login.goto_website();
  await UnloggedHome.navigate_through_menus();
});

test('Verify Toggle dark/light themes', async ({ page }) => {
  const Login = new LoginPage(page);
  const UnloggedHome = new UnloggedHomePage(page);

  await Login.goto_website();
  await UnloggedHome.verify_dark_or_light_themes();
});

test('Create a custom component', async ({ page }) => {
  const Login = new LoginPage(page);
  const UnloggedHome = new UnloggedHomePage(page);

  await Login.goto_website();
  await UnloggedHome.create_custom_component('calculator');
});