// @ts-check
const { test } = require('@playwright/test');
const {LoginPage} = require('../pages/login.js');


test('Successful Login using username', async ({ page }) => {
  const Login = new LoginPage(page);
  await Login.goto_website();
  await Login.access_login_page();
  await Login.fill_login('vtrmartinez', 'Str0ng_P@ss0rd');
  await Login.verify_user_logged('vtrmartinez@gmail.com');
});

test('Successful Login using email', async ({ page }) => {
  const Login = new LoginPage(page);
  await Login.goto_website();
  await Login.access_login_page();
  await Login.fill_login('vtrmartinez@gmail.com', 'Str0ng_P@ss0rd');
  await Login.verify_user_logged('vtrmartinez@gmail.com');
});